#!/bin/bash
# Modify the SSH welcome message
rm -f /etc/motd
cat >> /etc/motd << EOF

Welcome to the CentOS Docker-Linux !

Author : iLemonrain <ilemonrain@ilemonrain.com>
Project : https://hub.docker.com/r/ilemonrain/centos-sshd/
Docker Image : ilemonrain/centos-sshd

Linux Version : $(cat /etc/redhat-release)
Kernel Version : $(uname -r)
Hostname : $(uname -n)

Enjoy your Docker-Linux Node !

EOF
echo "Start Success !"
/usr/sbin/sshd -D
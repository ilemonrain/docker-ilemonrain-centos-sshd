### CentOS-SSHD on Docker ( A branch of Linux-on-Docker Project! )

### 0. 一点废话
受制于systend在Docker上的兼容性，CentOS 7.x系统将无法使用systemd相关程序 (如systemctl，service等)，如果后台启动服务的需要，请使用CentOS 6.x镜像，谢谢！(默认拉取的镜像为 CentOS 6.9)  
  
计划启动自动构建能力，目前已设置同步官方镜像 [CentOS](https://hub.docker.com/_/centos/) 作为触发条件，官方镜像更新 (docker push)，此镜像将会自动触发构建更新。  
     
### 1. 更新内容
Update 20180529
> 增加最新版本CentOS 7.5(由于官方镜像没有发布7.5版本，故使用centos:7作为基础镜像，通过系统升级的方式升级到CentOS 7.5)  
> 修改CentOS 6.9、CentOS 7.5镜像默认系统时区为Asia/Shanghai  
> CentOS 6.x系列，7.x系列非最新镜像（6.6~6.8和7.0~7.4）不再更新，如有需要请使用6.9与7.5版本镜像。  
  
### 2. 镜像标签 (Tag)
> **CentOS 7.5.1804** (```7.5```, ```7.5.1804```, ```7```)
> *CentOS 7.3.1611 (```7.3```, ```7.3.1611```)
> *CentOS 7.2.1511 (```7.2```, ```7.2.1511```)   
> *CentOS 7.1.1503 (```7.1```, ```7.1.1503```) 
> *CentOS 7.0.1406 (```7.0```, ```7.0.1406```)
> **CentOS 6.9** (```6.9```, ```6```, ```latest```)
> *CentOS 6.8 (```6.8```)
> *CentOS 6.7 (```6.7```)     
> *CentOS 6.6 (```6.6```) 
  
*：这些镜像已不再更新，请使用同系列最高版本镜像取代。  
  
### 3. 镜像说明
此镜像基于 [CentOS](https://hub.docker.com/_/centos/) 官方镜像制作而成，集成**OpenSSH-Server、OpenSSH-Client、initscripts (可以使用service命令)、EPEL镜像源、wget、passwd、tar、unzip**，满足最低运行环境需求，如果其他需要，请自行使用yum install进行安装。  
  
推荐使用最新的镜像 (CentOS 6.9 与 CentOS 7.5)，受制于Docker本身机制，无法在yum update升级系统版本后reboot(重启)完成升级。所以除非特殊需要，否则建议你使用最新镜像，这样可以避免很多玄学性问题。  

### 4. 食用方法
启动命令行：  
```
docker run -d -p 2222:22 --name CentOS-Docker ilemonrain/centos-sshd  
```
  
参数说明：  
> **-d**：以Daemon(后台)模式启动镜像  
> **-p 2222:22**：暴露宿主机的2222端口，映射到容器内部的22端口 (SSH)  
> **--name CentOS-Docker**：容器名称  
> **ilemonrain/centos-sshd**：镜像名称  
     
SSH登录信息：  
> 用户名：**root**  
> 密码：**centos**  
   
请在成功登录到容器后，立即使用“passwd root”命令，修改密码，以防止容器被恶意入侵！  

### 5. 调戏作者（划去）BUG反馈与交流
> Email：ilemonrain@ilemonrain.com  
> Telegram：@ilemonrain


